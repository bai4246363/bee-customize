var API_BASE_URL = ""
var request = function request(url, method, data) {
  var _url = API_BASE_URL + url;
  var header = {
    // 'Content-Type': 'application/x-www-form-urlencoded'
  };
  return new Promise(function (resolve, reject) {
    wx.request({
      url: _url,
      method: method,
      data: data,
      header: header,
      success: function success(request) {
        resolve(request.data);
      },
      fail: function fail(error) {
        reject(error);
      },
      complete: function complete(aaa) {
        // 加载完成
      }
    });
  });
}

module.exports={
  init: function init(a) {
    API_BASE_URL = a;
  },
  request: request,
  userDetail: function userDetail(token) {
    return request('/user/detail', 'get', {
      token: token
    });
  },
  userAmount: function userAmount(token) {
    return request('/user/amount', 'get', {
      token: token
    });
  },
  checkToken: function checkToken(token) {
    return request('/user/check-token', 'get', {
      token: token
    });
  },
  login_wx: function login_wx(code) {
    return request('/user/wxapp/login', 'post', {
      code: code,
      type: 2
    });
  },
  register_complex: function register_complex(data) {
    return request('/user/wxapp/register/complex', 'post', data);
  },
  goodsCategory: function goodsCategory() {
    return request('/shop/goods/category/all', 'get');
  },
  goods: function goods(data) {
    return request('/shop/goods/list', 'post', data);
  },
  shippingCarInfo: function shippingCarInfo(token) {
    return request('/shopping-cart/info', 'get', {
      token: token
    });
  },
  shippingCarInfoAddItem: function shippingCarInfoAddItem(token, goodsId, number, sku, addition) {
    return request('/shopping-cart/add','post', {
      token: token,
      goodsId: goodsId,
      number: number,
      sku: sku && sku.length > 0 ? JSON.stringify(sku) : '',
      addition: addition && addition.length > 0 ? JSON.stringify(addition) : ''
    });
  },
  shippingCarInfoRemoveItem: function shippingCarInfoRemoveItem(token, id) {
    return request('/shopping-cart/remove',  'post', {
      token: token, goodsId: id
    });
  },
  shippingCarInfoModifyNumber: function shippingCarInfoModifyNumber(token, id, number) {
    return request('/shopping-cart/modifyNumber',  'post', {
      token: token, goodsId: id, number: number
    });
  },
  shippingCarInfoRemoveAll: function shippingCarInfoRemoveAll(token) {
    return request('/shopping-cart/empty', 'post', {
      token: token
    });
  },
  orderCreate: function orderCreate(data) {
    return request('/order/create', 'post', data);
  },
  province: function province() {
    return request('/common/region/v2/province', 'get');
  },
  nextRegion: function nextRegion(pid) {
    return request('/common/region/v2/child', 'get', {
      pid: pid
    });
  },
  addAddress: function addAddress(data) {
    return request('/user/shipping-address/add','post', data);
  },
  queryAddress: function queryAddress(token) {
    return request('/user/shipping-address/list','get', {
      token: token
    });
  },
  deleteAddress: function deleteAddress(token, id) {
    return request('/user/shipping-address/delete','post', {
      id: id,
      token: token
    });
  },
  defaultAddress: function defaultAddress(token) {
    return request('/user/shipping-address/default/v2', 'get', {
      token: token
    });
  },
  orderList: function orderList(data) {
    return request('/order/list','post', data);
  },
  orderClose: function orderClose(token, orderId) {
    return request('/order/close', 'post', {
      orderId: orderId,
      token: token
    });
  },
  orderDelete: function orderDelete(token, orderId) {
    return request('/order/delete','post', {
      orderId: orderId,
      token: token
    });
  },
  queryConfigBatch: function queryConfigBatch(keys) {
    return request('/config/values', 'get', { keys: keys });
  },
  fetchShops: function fetchShops(data) {
    return request('/shop/subshop/list', 'post', data);
  },
  shopSubdetail: function shopSubdetail(id) {
    return request('/shop/subshop/detail/v2', 'get', { id: id });
  },
  banners: function banners(data) {
    return request('/banner/list','get', data);
  },
  addressDetail: function addressDetail(token, id) {
    return request('/user/shipping-address/detail/v2', 'get', {
      id: id,
      token: token
    });
  },
  bindMobileWxapp: function bindMobileWxapp(token, code, encryptedData, iv) {
    var pwd = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';

    return request('/user/wxapp/bindMobile', 'post', {
      token: token, code: code, encryptedData: encryptedData, iv: iv, pwd: pwd
    });
  },
  
}